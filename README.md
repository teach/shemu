Shemu
=====

When harvest comes, Shemu tells you if you have correctly submitted your work.

Shemu is a HTML interface looking for an extracted tarball for each students in
each projects.


Compilation
-----------

This program is written in Go. It has no dependancy, you can compile it with
the following command:

```shell
go build
```


Projects
--------

Shemu takes as argument a path to a directory containing projects.

The tree exptected looks like:

    rendu/
	    TP1/
		    login_x/
			    ...
		    mercie_d/
			    ...
	    TP2/
		    login_x/
			    ...
		    mercie_d/
			    ...

In the above tree, 2 projects are defined: named `TP1` and `TP2`, from their
directory name.


Students
--------

A file with students list is expected, in CSV format.

A line looks like:

```csv
Mercier,Pierre-Olivier,mercie_d,64170,github@nemunai.re,0123456789
```

The third column is used has student identifier. It is this identifier that is
search in each project directory.


Usage
-----

```shell
./shemu -students ~/projects/students.csv -path ~/projects/
```

Another convinient way to use it is to launch and forget:

```shell
nohup ./shemu -bind :8079 -title "Submissions title" -students ~/projects/students.csv -path ~/projects/ &
```
